﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MHanan
{
    public partial class Login : Form
    {
        Database db = new Database();
        FormController f = new FormController();

        public Login()
        {
            InitializeComponent();
        }

        public void doLogin()
        {
            try
            {
                Menu m = new Menu();
                DataTable dt = db.getData($"SELECT * FROM admin WHERE username = '{txtUsername.Text}' AND password = '{txtPassword.Text}' LIMIT 1");
                if (dt.Rows.Count == 1) {
                    f.redirect(this, m);
                }
                else {
                    MessageBox.Show("Username || Password Salah");
                }
            }catch(Exception e)
            {
                MessageBox.Show("An error has occured");
            }

        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            doLogin();
        }

        private void Login_FormClosing(object sender, FormClosingEventArgs e)
        {
            f.exit1(e);
        }
    }
}
