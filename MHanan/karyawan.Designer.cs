﻿namespace MHanan
{
    partial class karyawan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataKaryawan = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.txtNip = new System.Windows.Forms.TextBox();
            this.txtNama = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.rbMenikah = new System.Windows.Forms.RadioButton();
            this.rbBelumMenikah = new System.Windows.Forms.RadioButton();
            this.cbJabatan = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.numJumlahAnak = new System.Windows.Forms.NumericUpDown();
            this.numJamLembur = new System.Windows.Forms.NumericUpDown();
            this.btnSimpan = new System.Windows.Forms.Button();
            this.btnHapus = new System.Windows.Forms.Button();
            this.btnBersih = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnKeluar = new System.Windows.Forms.Button();
            this.btnCari = new System.Windows.Forms.Button();
            this.txtCari = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.lblTunjanganIstri = new System.Windows.Forms.Label();
            this.lblGajiKotor = new System.Windows.Forms.Label();
            this.lblPajak = new System.Windows.Forms.Label();
            this.lblGajiLembur = new System.Windows.Forms.Label();
            this.lblGajiBersih = new System.Windows.Forms.Label();
            this.lblTunjanganAnak = new System.Windows.Forms.Label();
            this.lblGajiPokok = new System.Windows.Forms.Label();
            this.lbla = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataKaryawan)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numJumlahAnak)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numJamLembur)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataKaryawan
            // 
            this.dataKaryawan.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataKaryawan.Location = new System.Drawing.Point(12, 377);
            this.dataKaryawan.Name = "dataKaryawan";
            this.dataKaryawan.Size = new System.Drawing.Size(1070, 416);
            this.dataKaryawan.TabIndex = 0;
            this.dataKaryawan.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataKaryawan_CellClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(47, 113);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "NIP";
            // 
            // txtNip
            // 
            this.txtNip.Location = new System.Drawing.Point(177, 110);
            this.txtNip.Name = "txtNip";
            this.txtNip.Size = new System.Drawing.Size(198, 27);
            this.txtNip.TabIndex = 2;
            // 
            // txtNama
            // 
            this.txtNama.Location = new System.Drawing.Point(177, 143);
            this.txtNama.Name = "txtNama";
            this.txtNama.Size = new System.Drawing.Size(198, 27);
            this.txtNama.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(47, 146);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 20);
            this.label2.TabIndex = 3;
            this.label2.Text = "Nama";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(47, 284);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 20);
            this.label3.TabIndex = 5;
            this.label3.Text = "Jumlah Anak";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(47, 179);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 20);
            this.label4.TabIndex = 6;
            this.label4.Text = "Jabatan";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(47, 233);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 20);
            this.label5.TabIndex = 7;
            this.label5.Text = "Status";
            // 
            // rbMenikah
            // 
            this.rbMenikah.AutoSize = true;
            this.rbMenikah.Location = new System.Drawing.Point(27, 17);
            this.rbMenikah.Name = "rbMenikah";
            this.rbMenikah.Size = new System.Drawing.Size(83, 24);
            this.rbMenikah.TabIndex = 8;
            this.rbMenikah.TabStop = true;
            this.rbMenikah.Text = "Menikah";
            this.rbMenikah.UseVisualStyleBackColor = true;
            // 
            // rbBelumMenikah
            // 
            this.rbBelumMenikah.AutoSize = true;
            this.rbBelumMenikah.Location = new System.Drawing.Point(27, 47);
            this.rbBelumMenikah.Name = "rbBelumMenikah";
            this.rbBelumMenikah.Size = new System.Drawing.Size(129, 24);
            this.rbBelumMenikah.TabIndex = 9;
            this.rbBelumMenikah.TabStop = true;
            this.rbBelumMenikah.Text = "Belum Menikah";
            this.rbBelumMenikah.UseVisualStyleBackColor = true;
            // 
            // cbJabatan
            // 
            this.cbJabatan.FormattingEnabled = true;
            this.cbJabatan.Items.AddRange(new object[] {
            "Direktur",
            "Manager",
            "Kabag",
            "Sekretaris",
            "Karyawan",
            "OfficeBoy"});
            this.cbJabatan.Location = new System.Drawing.Point(177, 176);
            this.cbJabatan.Name = "cbJabatan";
            this.cbJabatan.Size = new System.Drawing.Size(198, 28);
            this.cbJabatan.TabIndex = 10;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbMenikah);
            this.groupBox1.Controls.Add(this.rbBelumMenikah);
            this.groupBox1.Location = new System.Drawing.Point(177, 200);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 77);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(47, 327);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(89, 20);
            this.label6.TabIndex = 12;
            this.label6.Text = "Jam Lembur";
            // 
            // numJumlahAnak
            // 
            this.numJumlahAnak.Location = new System.Drawing.Point(177, 283);
            this.numJumlahAnak.Name = "numJumlahAnak";
            this.numJumlahAnak.Size = new System.Drawing.Size(200, 27);
            this.numJumlahAnak.TabIndex = 13;
            // 
            // numJamLembur
            // 
            this.numJamLembur.Location = new System.Drawing.Point(177, 325);
            this.numJamLembur.Name = "numJamLembur";
            this.numJamLembur.Size = new System.Drawing.Size(200, 27);
            this.numJamLembur.TabIndex = 14;
            // 
            // btnSimpan
            // 
            this.btnSimpan.Location = new System.Drawing.Point(462, 110);
            this.btnSimpan.Name = "btnSimpan";
            this.btnSimpan.Size = new System.Drawing.Size(210, 27);
            this.btnSimpan.TabIndex = 15;
            this.btnSimpan.Text = "Simpan";
            this.btnSimpan.UseVisualStyleBackColor = true;
            this.btnSimpan.Click += new System.EventHandler(this.btnSimpan_Click);
            // 
            // btnHapus
            // 
            this.btnHapus.Location = new System.Drawing.Point(462, 179);
            this.btnHapus.Name = "btnHapus";
            this.btnHapus.Size = new System.Drawing.Size(102, 27);
            this.btnHapus.TabIndex = 16;
            this.btnHapus.Text = "Hapus";
            this.btnHapus.UseVisualStyleBackColor = true;
            this.btnHapus.Click += new System.EventHandler(this.btnHapus_Click);
            // 
            // btnBersih
            // 
            this.btnBersih.Location = new System.Drawing.Point(570, 146);
            this.btnBersih.Name = "btnBersih";
            this.btnBersih.Size = new System.Drawing.Size(102, 27);
            this.btnBersih.TabIndex = 17;
            this.btnBersih.Text = "Bersih";
            this.btnBersih.UseVisualStyleBackColor = true;
            this.btnBersih.Click += new System.EventHandler(this.btnBersih_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.Location = new System.Drawing.Point(462, 146);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(102, 27);
            this.btnEdit.TabIndex = 18;
            this.btnEdit.Text = "Edit";
            this.btnEdit.UseVisualStyleBackColor = true;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnKeluar
            // 
            this.btnKeluar.Location = new System.Drawing.Point(570, 179);
            this.btnKeluar.Name = "btnKeluar";
            this.btnKeluar.Size = new System.Drawing.Size(102, 27);
            this.btnKeluar.TabIndex = 19;
            this.btnKeluar.Text = "Keluar";
            this.btnKeluar.UseVisualStyleBackColor = true;
            this.btnKeluar.Click += new System.EventHandler(this.btnKeluar_Click);
            // 
            // btnCari
            // 
            this.btnCari.Location = new System.Drawing.Point(678, 334);
            this.btnCari.Name = "btnCari";
            this.btnCari.Size = new System.Drawing.Size(61, 27);
            this.btnCari.TabIndex = 20;
            this.btnCari.Text = "Cari";
            this.btnCari.UseVisualStyleBackColor = true;
            this.btnCari.Click += new System.EventHandler(this.btnCari_Click);
            // 
            // txtCari
            // 
            this.txtCari.Location = new System.Drawing.Point(462, 334);
            this.txtCari.Name = "txtCari";
            this.txtCari.Size = new System.Drawing.Size(210, 27);
            this.txtCari.TabIndex = 21;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe UI", 25F);
            this.label7.Location = new System.Drawing.Point(43, 28);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(348, 46);
            this.label7.TabIndex = 22;
            this.label7.Text = "Manajemen Karyawan";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lblGajiPokok);
            this.groupBox2.Controls.Add(this.lbla);
            this.groupBox2.Controls.Add(this.lblTunjanganAnak);
            this.groupBox2.Controls.Add(this.lblGajiBersih);
            this.groupBox2.Controls.Add(this.lblGajiLembur);
            this.groupBox2.Controls.Add(this.lblPajak);
            this.groupBox2.Controls.Add(this.lblGajiKotor);
            this.groupBox2.Controls.Add(this.lblTunjanganIstri);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Location = new System.Drawing.Point(770, 71);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(312, 290);
            this.groupBox2.TabIndex = 24;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Lain-lain";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(17, 82);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(113, 20);
            this.label8.TabIndex = 2;
            this.label8.Text = "Tunjangan Istri :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(17, 245);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(86, 20);
            this.label10.TabIndex = 4;
            this.label10.Text = "Gaji Bersih :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(17, 211);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(96, 20);
            this.label11.TabIndex = 5;
            this.label11.Text = "Gaji Lembur :";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(17, 179);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(50, 20);
            this.label12.TabIndex = 6;
            this.label12.Text = "Pajak :";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(17, 146);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(83, 20);
            this.label13.TabIndex = 7;
            this.label13.Text = "Gaji Kotor :";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(17, 116);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(122, 20);
            this.label14.TabIndex = 8;
            this.label14.Text = "Tunjangan Anak :";
            // 
            // lblTunjanganIstri
            // 
            this.lblTunjanganIstri.AutoSize = true;
            this.lblTunjanganIstri.Location = new System.Drawing.Point(167, 84);
            this.lblTunjanganIstri.Name = "lblTunjanganIstri";
            this.lblTunjanganIstri.Size = new System.Drawing.Size(17, 20);
            this.lblTunjanganIstri.TabIndex = 9;
            this.lblTunjanganIstri.Text = "0";
            // 
            // lblGajiKotor
            // 
            this.lblGajiKotor.AutoSize = true;
            this.lblGajiKotor.Location = new System.Drawing.Point(167, 146);
            this.lblGajiKotor.Name = "lblGajiKotor";
            this.lblGajiKotor.Size = new System.Drawing.Size(17, 20);
            this.lblGajiKotor.TabIndex = 10;
            this.lblGajiKotor.Text = "0";
            // 
            // lblPajak
            // 
            this.lblPajak.AutoSize = true;
            this.lblPajak.Location = new System.Drawing.Point(167, 179);
            this.lblPajak.Name = "lblPajak";
            this.lblPajak.Size = new System.Drawing.Size(17, 20);
            this.lblPajak.TabIndex = 11;
            this.lblPajak.Text = "0";
            // 
            // lblGajiLembur
            // 
            this.lblGajiLembur.AutoSize = true;
            this.lblGajiLembur.Location = new System.Drawing.Point(167, 211);
            this.lblGajiLembur.Name = "lblGajiLembur";
            this.lblGajiLembur.Size = new System.Drawing.Size(17, 20);
            this.lblGajiLembur.TabIndex = 12;
            this.lblGajiLembur.Text = "0";
            // 
            // lblGajiBersih
            // 
            this.lblGajiBersih.AutoSize = true;
            this.lblGajiBersih.Location = new System.Drawing.Point(167, 245);
            this.lblGajiBersih.Name = "lblGajiBersih";
            this.lblGajiBersih.Size = new System.Drawing.Size(17, 20);
            this.lblGajiBersih.TabIndex = 13;
            this.lblGajiBersih.Text = "0";
            // 
            // lblTunjanganAnak
            // 
            this.lblTunjanganAnak.AutoSize = true;
            this.lblTunjanganAnak.Location = new System.Drawing.Point(167, 118);
            this.lblTunjanganAnak.Name = "lblTunjanganAnak";
            this.lblTunjanganAnak.Size = new System.Drawing.Size(17, 20);
            this.lblTunjanganAnak.TabIndex = 14;
            this.lblTunjanganAnak.Text = "0";
            // 
            // lblGajiPokok
            // 
            this.lblGajiPokok.AutoSize = true;
            this.lblGajiPokok.Location = new System.Drawing.Point(167, 56);
            this.lblGajiPokok.Name = "lblGajiPokok";
            this.lblGajiPokok.Size = new System.Drawing.Size(17, 20);
            this.lblGajiPokok.TabIndex = 16;
            this.lblGajiPokok.Text = "0";
            // 
            // lbla
            // 
            this.lbla.AutoSize = true;
            this.lbla.Location = new System.Drawing.Point(17, 54);
            this.lbla.Name = "lbla";
            this.lbla.Size = new System.Drawing.Size(85, 20);
            this.lbla.TabIndex = 15;
            this.lbla.Text = "Gaji Pokok :";
            // 
            // karyawan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1094, 808);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.cbJabatan);
            this.Controls.Add(this.txtCari);
            this.Controls.Add(this.btnCari);
            this.Controls.Add(this.btnKeluar);
            this.Controls.Add(this.btnEdit);
            this.Controls.Add(this.btnBersih);
            this.Controls.Add(this.btnHapus);
            this.Controls.Add(this.btnSimpan);
            this.Controls.Add(this.numJamLembur);
            this.Controls.Add(this.numJumlahAnak);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtNama);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtNip);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataKaryawan);
            this.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "karyawan";
            this.Text = "Manajemen Karyawan";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.karyawan_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.dataKaryawan)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numJumlahAnak)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numJamLembur)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataKaryawan;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtNip;
        private System.Windows.Forms.TextBox txtNama;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.RadioButton rbMenikah;
        private System.Windows.Forms.RadioButton rbBelumMenikah;
        private System.Windows.Forms.ComboBox cbJabatan;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown numJumlahAnak;
        private System.Windows.Forms.NumericUpDown numJamLembur;
        private System.Windows.Forms.Button btnSimpan;
        private System.Windows.Forms.Button btnHapus;
        private System.Windows.Forms.Button btnBersih;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Button btnKeluar;
        private System.Windows.Forms.Button btnCari;
        private System.Windows.Forms.TextBox txtCari;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lblTunjanganAnak;
        private System.Windows.Forms.Label lblGajiBersih;
        private System.Windows.Forms.Label lblGajiLembur;
        private System.Windows.Forms.Label lblPajak;
        private System.Windows.Forms.Label lblGajiKotor;
        private System.Windows.Forms.Label lblTunjanganIstri;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblGajiPokok;
        private System.Windows.Forms.Label lbla;
    }
}