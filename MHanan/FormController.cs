﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MHanan
{
    class FormController
    {
        public void redirect(Form a, Form b)
        {
            a.Hide();
            b.Show();
        }
        public void exit(Form a)
        {
            DialogResult di = MessageBox.Show("Yakin ingin keluar ?", "Important", MessageBoxButtons.YesNo,
            MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
            if (di == DialogResult.Yes)
            {
                Application.ExitThread();
            }
        }
        public void exit1(FormClosingEventArgs e)
        {
            DialogResult di = MessageBox.Show("Yakin ingin keluar ?", "Important", MessageBoxButtons.YesNo,
            MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
            if (di == DialogResult.Yes)
            {
                Application.ExitThread();
            }
            else
            {
                e.Cancel = true;
            }
        }
    }
}
