﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Windows.Forms;
using System.Data;

namespace MHanan
{
    class Database
    {
        MySqlCommand cmd;
        MySqlConnection con;
        MySqlDataAdapter adp;
        String config = "SERVER=localhost; DATABASE=mhanan; UID=root";

        public void connect()
        {
            try
            {
                con = new MySqlConnection(config);
                con.Open();
            }catch(Exception e)
            {
                MessageBox.Show("Couldn't connect to the server => " + e.Message);
            }
        }

        public void disconnect() => con.Close();

        public DataTable getData(String query)
        {
            DataTable dt = new DataTable();
            connect();
            try
            {
                adp = new MySqlDataAdapter(query, con);
                adp.Fill(dt);
            }catch(Exception e)
            {
                MessageBox.Show("Couldn't get data => "+e.Message + "=>" + query);
            }
            return dt;
        }

        public void run(String query)
        {
            try
            {
                connect();
                cmd = new MySqlCommand(query, con);
                cmd.ExecuteNonQuery();
                disconnect();
            }catch(Exception e)
            {
                MessageBox.Show("An error has occured, please contact your administrator => " + e.Message +"=>"+ query);
            }
        }

    }
}
