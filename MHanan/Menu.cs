﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MHanan
{
    public partial class Menu : Form
    {
        FormController f = new FormController();
        public Menu()
        {
            InitializeComponent();
        }

        private void gajiKaryawanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            karyawan k = new karyawan();
            f.redirect(this, k);
        }

        private void Menu_FormClosing(object sender, FormClosingEventArgs e)
        {
            f.exit1(e);
        }

        private void keluarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            f.exit(this);
        }
    }
}
