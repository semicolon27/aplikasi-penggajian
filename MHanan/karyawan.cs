﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MHanan
{
    public partial class karyawan : Form
    {
        FormController f = new FormController();
        Database db = new Database();

        public karyawan()
        {
            InitializeComponent();
            getData("1");
        }

        public void getData(String a)
        {
            String cari = txtCari.Text;
            String sql = $"SELECT *, gaji_pokok * 20/100 AS tunjangan_istri, gaji_pokok * 15/100 * jumlah_anak AS tunjangan_anak, gaji_pokok + gaji_pokok * 20/100 + gaji_pokok * 15/100 * jumlah_anak AS gaji_kotor, " +
                         $"(gaji_pokok + gaji_pokok * 20/100 + gaji_pokok * 15/100 * jumlah_anak) * 10/100 AS pajak, " +
                         $"jam_lembur * (gaji_pokok * 1/100) AS gaji_jam_lembur," +
                         $"((gaji_pokok + (gaji_pokok * 20/100) + gaji_pokok * 15/100 * jumlah_anak) - ((gaji_pokok + gaji_pokok * 20/100 + gaji_pokok * 15/100 * jumlah_anak) * 1/100) + (jam_lembur * (gaji_pokok * 1/100))) AS gaji_bersih" +
                         $" FROM karyawan a INNER JOIN jabatan b ON a.kode_jabatan = b.kode_jabatan ";
                        //$"nip LIKE '%{cari}%' OR nama LIKE '%{cari}%' OR a.kode_jabatan LIKE '%{cari}%' OR status LIKE '%{cari}%' OR " +
                        //$"jumlah_anak LIKE '%{cari}%' OR jam_lembur LIKE '%{cari}%' OR a.kode_jabatan LIKE '%{cari}%' OR jabatan LIKE '%{cari}%'" +
                        //$"OR gaji_pokok LIKE '%{cari}%' OR tunjangan_istri LIKE '%{cari}%' OR tunjangan_anak LIKE '%{cari}%' OR gaji_kotor LIKE '%{cari}%' OR pajak LIKE '%{cari}%' OR gaji_jam_lembur LIKE '%{cari}%' OR gaji_bersih LIKE '%{cari}%' ";
            String sql1 =$"WHERE nip LIKE '%{cari}%' OR nama LIKE '%{cari}%' OR status LIKE '%{cari}%' OR " +
                         $"jumlah_anak LIKE '%{cari}%' OR jam_lembur LIKE '%{cari}%' OR jabatan LIKE '%{cari}%' OR gaji_pokok LIKE '%{cari}%'";
            try
            {
                dataKaryawan.DataSource = db.getData(a == "1" ? sql : sql+sql1);
            }catch(Exception e)
            {
                MessageBox.Show(sql);
                Console.WriteLine(sql);
            }
            
        }

        public void postData()
        {
            txtNip.Enabled = true;
            db.run($"INSERT INTO karyawan VALUES('{txtNip.Text}', '{txtNama.Text}', '{getJabatan()}', '{getStatus()}', '{numJumlahAnak.Value}', '{numJamLembur.Value}', current_timestamp() )");
            getData("1");
        }

        public void putData()
        {
            txtNip.Enabled = true;
            db.run($"UPDATE karyawan SET nama = '{txtNama.Text}', kode_jabatan = '{getJabatan()}', status = '{getStatus()}', jumlah_anak = '{numJumlahAnak.Value}', jam_lembur = '{numJamLembur.Value}' WHERE nip = '{txtNip.Text}'");
            getData("1");
        }

        public void deleteData()
        {
            txtNip.Enabled = true;
            db.run($"DELETE FROM karyawan WHERE nip = {txtNip.Text}");
            getData("1");
        }

        public String getJabatan()
        {
            String a = cbJabatan.SelectedItem.ToString();
            String r = null;
            switch (a)
            {
                case "Direktur":
                    r = "KJ-01";
                    break;
                case "Manager":
                    r = "KJ-02";
                    break;
                case "Kabag":
                    r = "KJ-03";
                    break;
                case "Sekretaris":
                    r = "KJ-04";
                    break;
                case "Karyawan":
                    r = "KJ-05";
                    break;
                case "OfficeBoy":
                    r = "KJ-06";
                    break;
            }
            return r;
        }

        public String getStatus()
        {
            String r = null;
            if(!rbBelumMenikah.Checked && rbMenikah.Checked)
            {
                r = "1";
            }else if (rbBelumMenikah.Checked && !rbMenikah.Checked)
            {
                r = "0";
            }
            return r;
        }

        public void bersih()
        {
            txtNip.Text = txtNama.Text = cbJabatan.Text = txtCari.Text = null;
            numJamLembur.Value = numJumlahAnak.Value = 0;
            rbBelumMenikah.Checked = rbMenikah.Checked = false;
        }

        private void karyawan_FormClosing(object sender, FormClosingEventArgs e)
        {
            f.exit1(e);
        }

        private void btnKeluar_Click(object sender, EventArgs e)
        {
            f.exit(this);
        }

        private void btnBersih_Click(object sender, EventArgs e)
        {
            bersih();
        }

        private void btnSimpan_Click(object sender, EventArgs e)
        {
            postData();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            putData();
        }

        private void btnHapus_Click(object sender, EventArgs e)
        {
            deleteData();
            bersih();
        }

        private void dataKaryawan_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                txtNip.Enabled = false;
                var row = (dataKaryawan.DataSource as DataTable).Rows[e.RowIndex];
                int gp = int.Parse(row["gaji_pokok"].ToString());
                int anak = (int.Parse(row["jumlah_anak"].ToString()) <= 2 ? int.Parse(row["jumlah_anak"].ToString()) : 2);
                txtNip.Text = row["nip"].ToString();
                txtNama.Text = row["nama"].ToString();
                cbJabatan.SelectedItem = row["jabatan"].ToString();
                rbBelumMenikah.Checked = (row["status"].ToString() == "0");
                rbMenikah.Checked = (row["status"].ToString() == "1");
                numJumlahAnak.Value = row["status"].ToString() == "1" ? int.Parse(row["jumlah_anak"].ToString()) : 0;
                numJamLembur.Value = int.Parse(row["jam_lembur"].ToString());
                lblGajiPokok.Text = "Rp." + row["gaji_pokok"].ToString() + ",-";
                lblTunjanganIstri.Text = "Rp." + (gp * 20/100).ToString() + ",-";
                lblTunjanganAnak.Text = "Rp." + (anak * gp * 15/100).ToString() + ",-";
                lblGajiKotor.Text = "Rp." + (gp + (gp * 20 / 100) + (anak * gp * 15 / 100)).ToString() + ",-";
                lblPajak.Text = "Rp." + ((gp + (gp * 20 / 100) + (anak * gp * 15 / 100)) * 10/100).ToString() + ",-";
                lblGajiLembur.Text = "Rp." + (int.Parse(row["jam_lembur"].ToString()) * (int.Parse(row["gaji_pokok"].ToString()) * 1/100)).ToString() + ",-";
                lblGajiBersih.Text = "Rp." + ((int.Parse(row["jam_lembur"].ToString()) * (int.Parse(row["gaji_pokok"].ToString()) * 1 / 100)) + gp - ((gp + (gp * 20 / 100) + (anak * gp * 15 / 100)) * 10 / 100)).ToString() + ",-";
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
            }
        }

        private void btnCari_Click(object sender, EventArgs e)
        {
            getData("cari");
        }
    }
}
